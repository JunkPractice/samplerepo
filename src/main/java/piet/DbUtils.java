package piet;

import java.sql.*;

public class DbUtils {
    private String dbUrl, username, password, query;
    private DbType dbType;
    public Connection connection = null;
    public Statement statement = null;

    public DbUtils(String dbUrl, String username, String password, String query, DbType dbType) {
        this.dbUrl = dbUrl;
        this.username = username;
        this.password = password;
        this.query = query;
        this.dbType = dbType;
    }

    public void factory() {
        switch (dbType) {
            case RDBMS:
                Rdbms rdbms = new Rdbms("create database Employeedata"+"use Employeedata"+"create table employees" + "emp_id int PRIMARY KEY AUTO_INCREMENT ,name varchar(30)"+"position varchar(30)","","","");
                rdbms.create();
                break;
            case MONGO:
                Mongo mongo = new Mongo();

                break;
            case CASSANDRA:
                Cassandra cassandra = new Cassandra();
                break;
            default:
                System.out.println("Database not available");
        }
    }

    public void ConnectionDatabase() {
        try {
            connection = DriverManager.getConnection(dbUrl, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSet rs;
        try {
            rs = statement.executeQuery(query);

            while (rs.next()) {
                System.out.println(rs.getString("hospital_name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }

    }
}
