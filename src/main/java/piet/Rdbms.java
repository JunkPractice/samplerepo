package piet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Rdbms implements iCrud,iConnect {
    private Connection connection;
    private Statement statement;
    private String create,read,update,delete;

    public Rdbms(String create, String read, String update, String delete) {
        this.create = create;
        this.read = read;
        this.update = update;
        this.delete = delete;
    }

    @Override
    public void createConnection() {
        try {
            connection =DriverManager.getConnection("jdbc:mysql://localhost:3306/project");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionStatus() {

    }

    @Override
    public void stopConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create() {
        try {
            statement = connection.createStatement();
            statement.executeQuery(create);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void read() {
        try {
            statement=connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();

            try {
                statement.execute(read);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void update() {
        try {
            statement=connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();

            try {
                statement.execute(update);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void delete() {
        try {
            statement=connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            statement.execute(delete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
