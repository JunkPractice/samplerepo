package piet;

public interface iCrud {
    void create();
    void read();
    void update();
    void delete();
}
