package piet;

public interface iConnect {
    void createConnection();
    void connectionStatus();
    void stopConnection();
}
